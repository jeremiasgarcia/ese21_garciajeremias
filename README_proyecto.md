## Proyecto: Simulador de Señales Biológicas para Testing de Equipos Médicos.

### Objetivo General: 
Implementar un dispositivo que genere, de manera analógica, señales del tipo biológicas con distintas caracteristicas y en los valores de tensión adecuados a fin de ser conectados a las entradas de registro de biopotenciales de instrumentos medicos, para el contraste de su correcto funcionamiento.
    
### Objetivos Específicos:

- Lograr la implemtación de drivers para implementar el almacenamiento y posterior lectura de las señales a repropducir en una memoria SD.
- Lograr la implementación de drivers para la reproducción de la señal generada en una pantalla TFT embebida en el mismo dispositivo, en tiempo real.
- Desarrollar el firmware para el control de la pantalla TFT y de la interación con la memoria SD, sobre la plata EDU-CIAA-NXP.
- Implementar un menu de usuario básico que permita seleccionar entre dos señales distintas a reproducir.

### Diagrama en Bloques:

![Diagrama en Bloques](diagrama_en_bloques.png "Diagrama en Bloques.")

### Hardware Requerido:

- Módulo TFT 2.4" Tactil con lector de Tarjetas MicroSD.
    - Pantalla con controlador LCD ILI9341 (https://electronicavm.files.wordpress.com/2015/03/datasheet-chip-lcd-ili9341.pdf) 320×240 pixeles de resolución y 262.000 colores.
    - Panél táctil resistivo montado sobre la pantalla.
    - Soporte físico para tarjeta MicroSD con comunicación SPI.
- Placa de desarrollo EDU-CIAA-NXP.
    - Microcontrolador LPC4337 (Dual Core M4/M0).
    - USB-DEBUG implementado con FT2232HL.
    - Periféricos On-Board (4 pulsadores, 3 leds simples y 1 led RGB). 
