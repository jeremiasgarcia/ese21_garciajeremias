/** @addtogroup ecg_plot
 *  @{
 *  @file ecg_plot.h
 *  @brief Declaraciones de funciones y constantes simbólicas correspondientes a la aplicación.
 *  @date 18/09/2021
 */

#ifndef _ECG_PLOT_H
#define _ECG_PLOT_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);
void SisInit(void);
//void send_ecg(void);
//void switch_ecg(void);
//void OnOff_ecg(void);
//void faster(void);
//void lower(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _ECG_PLOT_H */

