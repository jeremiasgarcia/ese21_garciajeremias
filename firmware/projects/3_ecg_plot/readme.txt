3_ecg_plot

Reproduce una señal de ECG almacenada en FLASH por el puerto Serie.
Mediante el valor de un potenciometro leído por ADC, puede modificarse la amplitud de la señal
enviada.
