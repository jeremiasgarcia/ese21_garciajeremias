/*! \mainpage Graficador de ECG.
 * \date 18/09/2021
 * \author Jeremías A. García Cabrera
 * \section genDesc Descripción general
 *
 *	La presente aplicación implementa un gratificador de ECG, basado en dos señales
 *	electrocardiográficas almacenadas en flash, y reproducidas mediante el puerto
 *	serie. Como interfaz gráfica se utiliza el programa "SerialPlot" a los fines de
 *	recibir y presentar las gráficas.
 *
 *	\section desarrolloObs Observaciones generales
 *
 *	EL programa cuentas con dos funcionalidades básicas para el usuario, por un lado
 *	un botón de ON-OFF implementada con la Tecla 1, que inicia o para la emisión de
 *	datos por el puerto serie. Por otro lado con la Tecla 2, el usuario puede cambiar
 *	la señal de ECG que se esta reproduciendo, intercambiando entre una u otro con
 *	cada acción de la mencionada tecla.
 *
 * \section changelog Registro de cambios
 *
 * |   Fecha    | Descripción                                      |
 * |:----------:|:-------------------------------------------------|
 * | 18/09/2021 | Creación de la aplicación                        |
 * | 21/09/2021 | Generación de documentación                      |
 */

/*==================[inclusions]=============================================*/
#include "ecg_plot.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include "switch.h"
#include "analog_io.h"
/*==================[macros and definitions]=================================*/
#define ECG1_last 231
#define ECG2_last 256
/*==================[internal data definition]===============================*/
const uint8_t ECG1[] = {
		76,77,78,77,79,86,81,76,84,93,85,80,//12
		89,95,89,85,93,98,94,88,98,105,96,91,//12
		99,105,101,96,102,106,101,96,100,107,101,//11
		94,100,104,100,91,99,103,98,91,96,105,95,//12
		88,95,100,94,85,93,99,92,84,91,96,87,80,//13
		83,92,86,78,84,89,79,73,81,83,78,70,80,82,//14
		79,69,80,82,81,70,75,81,77,74,79,83,82,72,//14
		80,87,79,76,85,95,87,81,88,83,88,84,87,94,//14
		86,82,85,94,85,82,85,95,86,83,92,99,91,88,//14
		94,98,95,90,97,105,104,94,98,114,117,124,144,//13
		180,210,236,253,227,171,99,49,34,29,43,69,89,//13
		89,90,98,107,104,98,104,110,102,98,103,111,101,//13
		94,103,108,102,95,97,106,100,92,101,103,100,94,98,//14
		103,96,90,98,103,97,90,99,104,95,90,99,104,100,93,//15
		100,106,101,93,101,105,103,96,105,112,105,99,103,108,//14
		99,96,102,106,99,90,92,100,87,80,82,88,77,69,75,79,//16
		74,67,71,78,72,67,73,81,77,71,75,84,79,77,77,76,76//17
};//Total 231 muestras.

const uint8_t ECG2[] = {
		17,17,17,17,17,17,17,17,17,17,17,18,18,18,17,17,17,17,//18
		17,17,17,18,18,18,18,18,18,18,17,17,16,16,16,16,17,17,//18
		18,18,18,17,17,17,17,18,18,19,21,22,24,25,26,27,28,29,//18
		31,32,33,34,34,35,37,38,37,34,29,24,19,15,14,15,16,17,//18
		17,17,16,15,14,13,13,13,13,13,13,13,12,12,10,6,2,3,15,//19
		43,88,145,199,237,252,242,211,167,117,70,35,16,14,22,32,//16
		38,37,32,27,24,24,26,27,28,28,27,28,28,30,31,31,31,32,33,//19
		34,36,38,39,40,41,42,43,45,47,49,51,53,55,57,60,62,65,68,//19
		71,75,79,83,87,92,97,101,106,111,116,121,125,129,133,136,//16
		138,139,140,140,139,137,133,129,123,117,109,101,92,84,77,//15
		70,64,58,52,47,42,39,36,34,31,30,28,27,26,25,25,25,25,25,//19
		25,25,25,24,24,24,24,25,25,25,25,25,25,25,24,24,24,24,24,//19
		24,24,24,23,23,22,22,21,21,21,20,20,20,20,20,19,19,18,18,//19
		18,19,19,19,19,18,17,17,18,18,18,18,18,18,18,18,17,17,17,//19
		17,17,17,17//4
};//Total 256 muestras.

uint8_t * ECG[2] = {&ECG1,&ECG2};
uint16_t ECG_last[2] = {ECG1_last,ECG2_last};
uint8_t first_t = 1;
bool	timer_OnOff = 0;
bool	ecg_selec = 0;
/*==================[internal functions declaration]=========================*/
/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A,4,&send_ecg};
serial_config my_terminal = {SERIAL_PORT_PC,9600,NULL};
analog_input_config nivel_ecg = {CH0,AINPUTS_SINGLE_READ,NULL};
/*==================[external functions definition]==========================*/

void send_ecg(void)
{
	uint8_t ECG_out;
	uint8_t aux;
	uint16_t ECG_niv;
	static uint8_t i;
	if(first_t)
	{
		first_t = 0;
		i = 0;
	}
	if(i <= ECG_last[ecg_selec])
	{
		AnalogInputReadPolling(CH0,&ECG_niv);
		aux = 255/(ECG_niv>>2);
		ECG_out = *(ECG[ecg_selec]+i)/aux;
		UartSendByte(SERIAL_PORT_PC,&ECG_out);
		i++;
	}
	else
		i = 0;
}

void switch_ecg(void)
{
	if(ecg_selec)
		ecg_selec = 0;
	else
		ecg_selec = 1;
}

void OnOff_ecg(void)
{
	if(!timer_OnOff)
	{
		timer_OnOff = 1;
		TimerStart(TIMER_A);
	}
	else
	{
		timer_OnOff = 0;
		TimerStop(TIMER_A);
	}
}

void SisInit(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(&my_timer);
	UartInit(&my_terminal);
	AnalogInputInit(&nivel_ecg);
	SwitchActivInt(SWITCH_1, &OnOff_ecg);
	SwitchActivInt(SWITCH_2, &switch_ecg);
}

int main(void)
{
	SisInit();
    while(1)
    {}
}

/*==================[end of file]============================================*/

